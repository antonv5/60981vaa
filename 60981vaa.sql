-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Сен 16 2021 г., 21:44
-- Версия сервера: 8.0.23-0ubuntu0.20.04.1
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `60981vaa`
--

-- --------------------------------------------------------

--
-- Структура таблицы `Bike`
--

CREATE TABLE `Bike` (
  `id` int NOT NULL,
  `model` varchar(255) NOT NULL,
  `year` int NOT NULL,
  `wheel_size` int NOT NULL,
  `shock_absorber_id` int NOT NULL,
  `frame_size` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `number_of_speeds` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `brake_id` int NOT NULL,
  `user_id` int NOT NULL,
  `price` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `Bike`
--

INSERT INTO `Bike` (`id`, `model`, `year`, `wheel_size`, `shock_absorber_id`, `frame_size`, `number_of_speeds`, `brake_id`, `user_id`, `price`) VALUES
(1, 'Merida Matts', 2018, 26, 2, 'M', '3x7', 2, 0, ''),
(4, 'Scott Contessa', 2019, 29, 2, 'L', '3x9', 3, 0, ''),
(5, 'Cube Aim Race 29', 2020, 29, 2, 'L', '3x9', 2, 0, ''),
(6, 'Cube Aim SL 29', 2017, 29, 2, 'M', '3x9', 3, 0, ''),
(7, 'Cube Access WS EXC 29', 2018, 29, 1, 'L', '3x9', 2, 0, ''),
(8, 'Merida Matts', 2016, 26, 1, 'S', '3x7', 2, 0, '');

-- --------------------------------------------------------

--
-- Структура таблицы `brake`
--

CREATE TABLE `brake` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `brake`
--

INSERT INTO `brake` (`id`, `name`) VALUES
(1, 'rim vector'),
(2, 'mechanical disk'),
(3, 'hydraulic disc');

-- --------------------------------------------------------

--
-- Структура таблицы `rent`
--

CREATE TABLE `rent` (
  `id` int NOT NULL,
  `bike_id` int NOT NULL,
  `user_id` int NOT NULL,
  `date_time` date NOT NULL,
  `date_planned` date NOT NULL,
  `date_fact` date DEFAULT NULL,
  `credit` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `rent`
--

INSERT INTO `rent` (`id`, `bike_id`, `user_id`, `date_time`, `date_planned`, `date_fact`, `credit`) VALUES
(1, 4, 0, '2021-09-01', '2021-09-15', NULL, 1000);

-- --------------------------------------------------------

--
-- Структура таблицы `shock_absorber`
--

CREATE TABLE `shock_absorber` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `shock_absorber`
--

INSERT INTO `shock_absorber` (`id`, `name`) VALUES
(1, 'without'),
(2, 'front');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`) VALUES
(0, 'Ivan Ivanov');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `Bike`
--
ALTER TABLE `Bike`
  ADD PRIMARY KEY (`id`),
  ADD KEY `brake_id` (`brake_id`),
  ADD KEY `shock_absorber_id` (`shock_absorber_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `brake`
--
ALTER TABLE `brake`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `rent`
--
ALTER TABLE `rent`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bike_id` (`bike_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `shock_absorber`
--
ALTER TABLE `shock_absorber`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `Bike`
--
ALTER TABLE `Bike`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `brake`
--
ALTER TABLE `brake`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `rent`
--
ALTER TABLE `rent`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `shock_absorber`
--
ALTER TABLE `shock_absorber`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `Bike`
--
ALTER TABLE `Bike`
  ADD CONSTRAINT `Bike_ibfk_1` FOREIGN KEY (`brake_id`) REFERENCES `brake` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `Bike_ibfk_2` FOREIGN KEY (`shock_absorber_id`) REFERENCES `shock_absorber` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `Bike_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `rent`
--
ALTER TABLE `rent`
  ADD CONSTRAINT `rent_ibfk_1` FOREIGN KEY (`bike_id`) REFERENCES `Bike` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `rent_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
